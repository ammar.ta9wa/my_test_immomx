#from scrapy.conf import settings
from scrapy.exporters import CsvItemExporter
import csv

class MyProjectCsvItemExporter(CsvItemExporter):

    def __init__(self, *args, **kwargs):
        delimiter = settings.get('CSV_DELIMITER', ';')
        kwargs['delimiter'] = delimiter
        kwargs['quoting'] = csv.QUOTE_ALL
        super(MyProjectCsvItemExporter, self).__init__(*args, **kwargs)
