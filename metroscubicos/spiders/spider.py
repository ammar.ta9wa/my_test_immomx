# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf8')
import scrapy
import re
import phonenumbers
import csv
import datetime
from metroscubicos.mhelpers import extract_from, get_item
import neukolln.spiders
from neukolln.items import ImmoItem
from neukolln.utils import clean_text, find_french_zipcode, cast_float
import ast
from bs4 import BeautifulSoup

import logging

logger = logging.getLogger()
PRICE_MAX = 1690000000
PRICE_RANGE = 1000
class MetroscubicosSpider(neukolln.spiders.NeukollnBaseSpider, scrapy.Spider):
    #luminati_username = "lum-customer-autobiz-zone-pige_auto_generic_world"
    #luminati_password = "0f36h113HZBZdkcSQDcE"
    luminati_username ="lum-customer-autobiz-zone-zone_new-country-fr"
    luminati_password = "d0OlFBFpEbp5WeQ3Q0gl"
    luminati_allow_debug = False
    today = datetime.date.today()
    name_variable = 'metroscubicos'+(today.strftime("_%y_%m_%d"))
    name = name_variable
    neukolln_export_to_json = False
    allowed_domains = ['metroscubicos.com']
    start_urls = ['https://inmuebles.metroscubicos.com/aguascalientes/',
'https://inmuebles.metroscubicos.com/aguascalientes/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/aguascalientes/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/baja-california-sur/',
'https://inmuebles.metroscubicos.com/baja-california/',
'https://inmuebles.metroscubicos.com/baja-california/_PriceRange_0-500000_NoIndex_True',
'https://inmuebles.metroscubicos.com/baja-california/_PriceRange_3500000-0',
'https://inmuebles.metroscubicos.com/baja-california/_PriceRange_500000-3500000',
'https://inmuebles.metroscubicos.com/bodegas/coahuila/',
'https://inmuebles.metroscubicos.com/bodegas/distrito-federal/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/bodegas/distrito-federal/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/bodegas/estado-de-mexico/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/bodegas/estado-de-mexico/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/bodegas/guerrero/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/bodegas/guerrero/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/bodegas/hidalgo/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/bodegas/hidalgo/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/bodegas/jalisco/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/bodegas/jalisco/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/bodegas/morelos/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/bodegas/morelos/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/bodegas/nuevo-leon/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/bodegas/nuevo-leon/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/bodegas/puebla/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/bodegas/puebla/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/bodegas/queretaro/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/bodegas/queretaro/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/bodegas/quintana-roo/',
'https://inmuebles.metroscubicos.com/bodegas/veracruz/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/bodegas/veracruz/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/bodegas/yucatan/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/bodegas/yucatan/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/campeche/',
'https://inmuebles.metroscubicos.com/casas/chihuahua/_OrderId_PRICE_NoIndex_True',
'https://inmuebles.metroscubicos.com/casas/chihuahua/_OrderId_PRICE*DESC_NoIndex_True',
'https://inmuebles.metroscubicos.com/casas/coahuila/_PriceRange_0-1500000',
'https://inmuebles.metroscubicos.com/casas/coahuila/_PriceRange_1500000-2500000',
'https://inmuebles.metroscubicos.com/casas/coahuila/_PriceRange_2500000-0',
'https://inmuebles.metroscubicos.com/casas/distrito-federal_OrderId_PRICE*DESC_PriceRange_0-1000000_NoIndex_True_TOTAL*AREA_100m%C2%B2-250m%C2%B2',
'https://inmuebles.metroscubicos.com/casas/distrito-federal_OrderId_PRICE*DESC_PriceRange_1000000-2500000_NoIndex_True_TOTAL*AREA_100m%C2%B2-250m%C2%B2',
'https://inmuebles.metroscubicos.com/casas/distrito-federal_OrderId_PRICE*DESC_PriceRange_2500000-6500000_TOTAL*AREA_100m%C2%B2-250m%C2%B2',
'https://inmuebles.metroscubicos.com/casas/distrito-federal_OrderId_PRICE*DESC_PriceRange_6500000-15000000_NoIndex_True_TOTAL*AREA_200m%C2%B2-550m%C2%B2',
'https://inmuebles.metroscubicos.com/casas/distrito-federal_OrderId_PRICE*DESC_PriceRange_15000000-0_NoIndex_True_TOTAL*AREA_200m%C2%B2-550m%C2%B2',
'https://inmuebles.metroscubicos.com/casas/distrito-federal_OrderId_PRICE*DESC_PriceRange_6500000-0_TOTAL*AREA_550m%C2%B2-*',
'https://inmuebles.metroscubicos.com/casas/distrito-federal_OrderId_PRICE_PriceRange_6500000-0_NoIndex_True_TOTAL*AREA_550m%C2%B2-*',
'https://inmuebles.metroscubicos.com/casas/distrito-federal_OrderId_PRICE_PriceRange_0-1000000_NoIndex_True_TOTAL*AREA_100m%C2%B2-250m%C2%B2',
'https://inmuebles.metroscubicos.com/casas/distrito-federal_OrderId_PRICE_PriceRange_1000000-2500000_NoIndex_True_TOTAL*AREA_100m%C2%B2-250m%C2%B2',
'https://inmuebles.metroscubicos.com/casas/distrito-federal_OrderId_PRICE_PriceRange_2500000-6500000_TOTAL*AREA_100m%C2%B2-250m%C2%B2',
'https://inmuebles.metroscubicos.com/casas/distrito-federal_OrderId_PRICE_PriceRange_6500000-15000000_NoIndex_True_TOTAL*AREA_200m%C2%B2-550m%C2%B2',
'https://inmuebles.metroscubicos.com/casas/distrito-federal_OrderId_PRICE_PriceRange_15000000-0_NoIndex_True_TOTAL*AREA_200m%C2%B2-550m%C2%B2',
'https://inmuebles.metroscubicos.com/casas/distrito-federal_OrderId_PRICE_PriceRange_6500000-0_TOTAL*AREA_550m%C2%B2-*',
'https://inmuebles.metroscubicos.com/casas/distrito-federal_OrderId_PRICE*DESC_PriceRange_6500000-0_NoIndex_True_TOTAL*AREA_550m%C2%B2-*',
'https://inmuebles.metroscubicos.com/casas/distrito-federal_PriceRange_0-2500000_TOTAL*AREA_*-100m%C2%B2',
'https://inmuebles.metroscubicos.com/casas/distrito-federal_PriceRange_0-2500000_TOTAL*AREA_250m%C2%B2-*',
'https://inmuebles.metroscubicos.com/casas/distrito-federal_PriceRange_2500000-6500000_TOTAL*AREA_*-100m%C2%B2',
'https://inmuebles.metroscubicos.com/casas/distrito-federal_PriceRange_2500000-6500000_TOTAL*AREA_250m%C2%B2-*',
'https://inmuebles.metroscubicos.com/casas/distrito-federal_PriceRange_6500000-0_TOTAL*AREA_*-200m%C2%B2',
'https://inmuebles.metroscubicos.com/casas/estado-de-mexico_OrderId_PRICE*DESC_PriceRange_1000000-3500000_TOTAL*AREA_100m%C2%B2-200m%C2%B2',
'https://inmuebles.metroscubicos.com/casas/estado-de-mexico_OrderId_PRICE*DESC_PriceRange_3500000-0_TOTAL*AREA_250m%C2%B2-550m%C2%B2',
'https://inmuebles.metroscubicos.com/casas/estado-de-mexico_OrderId_PRICE_PriceRange_1000000-3500000_TOTAL*AREA_100m%C2%B2-200m%C2%B2',
'https://inmuebles.metroscubicos.com/casas/estado-de-mexico_OrderId_PRICE_PriceRange_3500000-0_TOTAL*AREA_250m%C2%B2-550m%C2%B2',
'https://inmuebles.metroscubicos.com/casas/estado-de-mexico_PriceRange_0-1000000_TOTAL*AREA_*-85m%C2%B2',
'https://inmuebles.metroscubicos.com/casas/estado-de-mexico_PriceRange_0-1000000_TOTAL*AREA_250m%C2%B2-*',
'https://inmuebles.metroscubicos.com/casas/estado-de-mexico_PriceRange_0-1000000_TOTAL*AREA_85m%C2%B2-250m%C2%B2',
'https://inmuebles.metroscubicos.com/casas/estado-de-mexico_PriceRange_1000000-3500000_TOTAL*AREA_*-100m%C2%B2',
'https://inmuebles.metroscubicos.com/casas/estado-de-mexico_PriceRange_1000000-3500000_TOTAL*AREA_200m%C2%B2-*',
'https://inmuebles.metroscubicos.com/casas/estado-de-mexico_PriceRange_3500000-0_TOTAL*AREA_*-250m%C2%B2',
'https://inmuebles.metroscubicos.com/casas/estado-de-mexico_OrderId_PRICE_PriceRange_3500000-0_NoIndex_True_TOTAL*AREA_550m%C2%B2-*',
'https://inmuebles.metroscubicos.com/casas/estado-de-mexico_OrderId_PRICE*DESC_PriceRange_3500000-0_NoIndex_True_TOTAL*AREA_550m%C2%B2-*',
'https://inmuebles.metroscubicos.com/casas/guerrero/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/casas/guerrero/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/casas/hidalgo/_OrderId_PRICE_PriceRange_0-1000000',
'https://inmuebles.metroscubicos.com/casas/hidalgo/_OrderId_PRICE_PriceRange_1000000-2000000',
'https://inmuebles.metroscubicos.com/casas/hidalgo/_OrderId_PRICE_PriceRange_2000000-0',
'https://inmuebles.metroscubicos.com/casas/jalisco/_OrderId_PRICE_PriceRange_0-2000000',
'https://inmuebles.metroscubicos.com/casas/jalisco/_OrderId_PRICE_PriceRange_2000000-5500000',
'https://inmuebles.metroscubicos.com/casas/jalisco/_OrderId_PRICE_PriceRange_5500000-0',
'https://inmuebles.metroscubicos.com/morelos/_OrderId_PRICE*DESC_PriceRange_0-65000',
'https://inmuebles.metroscubicos.com/morelos/_OrderId_PRICE*DESC_PriceRange_65000-900000',
'https://inmuebles.metroscubicos.com/morelos/_OrderId_PRICE*DESC_PriceRange_900000-1500000',
'https://inmuebles.metroscubicos.com/casas/morelos/_OrderId_PRICE*DESC_PriceRange_1500000-2000000',
'https://inmuebles.metroscubicos.com/casas/morelos/_OrderId_PRICE*DESC_PriceRange_2000000-3000000',
'https://inmuebles.metroscubicos.com/casas/morelos/_OrderId_PRICE*DESC_PriceRange_3000000-4000000',
'https://inmuebles.metroscubicos.com/casas/morelos/_OrderId_PRICE*DESC_PriceRange_4000000-6000000',
'https://inmuebles.metroscubicos.com/casas/morelos/_OrderId_PRICE*DESC_PriceRange_6000000-9500000',
'https://inmuebles.metroscubicos.com/casas/morelos/_OrderId_PRICE*DESC_PriceRange_9500000-0',
'https://inmuebles.metroscubicos.com/casas/nuevo-leon_OrderId_PRICE*DESC_PriceRange_0-2500000',
'https://inmuebles.metroscubicos.com/casas/nuevo-leon_OrderId_PRICE*DESC_PriceRange_2500000-8500000',
'https://inmuebles.metroscubicos.com/casas/nuevo-leon_OrderId_PRICE_PriceRange_0-2500000',
'https://inmuebles.metroscubicos.com/casas/nuevo-leon_OrderId_PRICE_PriceRange_2500000-8500000',
'https://inmuebles.metroscubicos.com/casas/nuevo-leon_OrderId_PRICE*DESC_PriceRange_8500000-10000000',
'https://inmuebles.metroscubicos.com/casas/nuevo-leon_OrderId_PRICE*DESC_PriceRange_10000000-15000000',
'https://inmuebles.metroscubicos.com/casas/nuevo-leon_OrderId_PRICE*DESC_PriceRange_15000000-0',
'https://inmuebles.metroscubicos.com/casas/puebla/_OrderId_PRICE_PriceRange_0-1500000',
'https://inmuebles.metroscubicos.com/casas/puebla/_OrderId_PRICE_PriceRange_1500000-3500000',
'https://inmuebles.metroscubicos.com/casas/puebla/_OrderId_PRICE_PriceRange_3500000-0',
'https://inmuebles.metroscubicos.com/casas/queretaro_OrderId_PRICE*DESC_PriceRange_1500000-3000000_COVERED*AREA_*-150m%C2%B2',
'https://inmuebles.metroscubicos.com/casas/queretaro_OrderId_PRICE*DESC_PriceRange_3000000-0_TOTAL*AREA_*-250m%C2%B2',
'https://inmuebles.metroscubicos.com/casas/queretaro_OrderId_PRICE*DESC_PriceRange_3000000-0_TOTAL*AREA_250m%C2%B2-350m%C2%B2',
'https://inmuebles.metroscubicos.com/casas/queretaro_OrderId_PRICE*DESC_PriceRange_3000000-0_TOTAL*AREA_350m%C2%B2-*',
'https://inmuebles.metroscubicos.com/casas/queretaro_OrderId_PRICE_PriceRange_1500000-3000000_COVERED*AREA_*-150m%C2%B2',
'https://inmuebles.metroscubicos.com/casas/queretaro_OrderId_PRICE_PriceRange_1500000-3000000_COVERED*AREA_150m%C2%B2-200m%C2%B2',
'https://inmuebles.metroscubicos.com/casas/queretaro_OrderId_PRICE_PriceRange_3000000-0_TOTAL*AREA_*-250m%C2%B2',
'https://inmuebles.metroscubicos.com/casas/queretaro_OrderId_PRICE_PriceRange_3000000-0_TOTAL*AREA_250m%C2%B2-350m%C2%B2',
'https://inmuebles.metroscubicos.com/casas/queretaro_OrderId_PRICE_PriceRange_3000000-0_TOTAL*AREA_350m%C2%B2-*',
'https://inmuebles.metroscubicos.com/casas/queretaro_PriceRange_1500000-3000000_COVERED*AREA_200m%C2%B2-*',
'https://inmuebles.metroscubicos.com/casas/quintana-roo/_PriceRange_0-1500000',
'https://inmuebles.metroscubicos.com/casas/quintana-roo/_PriceRange_1500000-3500000',
'https://inmuebles.metroscubicos.com/casas/quintana-roo/_PriceRange_3500000-6000000',
'https://inmuebles.metroscubicos.com/casas/quintana-roo/_PriceRange_6000000-0',
'https://inmuebles.metroscubicos.com/casas/renta/queretaro_PriceRange_0-1500000',
'https://inmuebles.metroscubicos.com/casas/venta/guanajuato/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/casas/venta/queretaro_OrderId_PRICE*DESC_PriceRange_0-1500000',
'https://inmuebles.metroscubicos.com/casas/venta/queretaro_OrderId_PRICE_PriceRange_0-1500000',
'https://inmuebles.metroscubicos.com/casas/veracruz/_OrderId_PRICE_PriceRange_1500000-2000000',
'https://inmuebles.metroscubicos.com/casas/veracruz/_OrderId_PRICE_PriceRange_2000000-3500000',
'https://inmuebles.metroscubicos.com/casas/veracruz/_OrderId_PRICE_PriceRange_3500000-0',
'https://inmuebles.metroscubicos.com/casas/veracruz/_OrderId_PRICE*DESC_PriceRange_3500000-0',
'https://inmuebles.metroscubicos.com/casas/veracruz/_PriceRange_0-850000',
'https://inmuebles.metroscubicos.com/casas/veracruz/_PriceRange_850000-1500000',
'https://inmuebles.metroscubicos.com/casas/yucatan/_OrderId_PRICE*DESC_PriceRange_1500000-2000000',
'https://inmuebles.metroscubicos.com/casas/yucatan/_OrderId_PRICE*DESC_PriceRange_2000000-2500000',
'https://inmuebles.metroscubicos.com/casas/yucatan/_OrderId_PRICE_PriceRange_2000000-2500000',
'https://inmuebles.metroscubicos.com/casas/yucatan/_OrderId_PRICE_PriceRange_2500000-2800000',
'https://inmuebles.metroscubicos.com/casas/yucatan/_OrderId_PRICE_PriceRange_2800000-3200000',
'https://inmuebles.metroscubicos.com/casas/yucatan/_OrderId_PRICE_PriceRange_3200000-3800000',
'https://inmuebles.metroscubicos.com/casas/yucatan/_OrderId_PRICE_PriceRange_3800000-4500000',
'https://inmuebles.metroscubicos.com/casas/yucatan/_OrderId_PRICE_PriceRange_4500000-6000000',
'https://inmuebles.metroscubicos.com/casas/yucatan/_OrderId_PRICE_PriceRange_6000000-0',
'https://inmuebles.metroscubicos.com/casas/yucatan/_PriceRange_0-1500000',
'https://inmuebles.metroscubicos.com/casasqueretaro_OrderId_PRICE_PriceRange_1500000-3000000_COVERED*AREA_150m%C2%B2-200m%C2%B2',
'https://inmuebles.metroscubicos.com/casasqueretaro_OrderId_PRICE*DESC_PriceRange_1500000-3000000_COVERED*AREA_150m%C2%B2-200m%C2%B2',
'https://inmuebles.metroscubicos.com/chiapas/',
'https://inmuebles.metroscubicos.com/chiapas/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/chiapas/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/chihuahua/_PriceRange_0-800000',
'https://inmuebles.metroscubicos.com/chihuahua/_PriceRange_800000-3500000',
'https://inmuebles.metroscubicos.com/chihuahua/_PriceRange_3500000-0',
'https://inmuebles.metroscubicos.com/chihuahua/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/chihuahua/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/coahuila/',
'https://inmuebles.metroscubicos.com/colima/',
'https://inmuebles.metroscubicos.com/cuartos/distrito-federal/',
'https://inmuebles.metroscubicos.com/departamentos/coahuila/',
'https://inmuebles.metroscubicos.com/departamentos/desarrollos-inmobiliarios/distrito-federal_PriceRange_3000000-0_COVERED*AREA_90m%C2%B2-150m%C2%B2',
'https://inmuebles.metroscubicos.com/departamentos/desarrollos-inmobiliarios/distrito-federal_PriceRange_750000-3000000_COVERED*AREA_60m%C2%B2-80m%C2%B2',
'https://inmuebles.metroscubicos.com/departamentos/distrito-federal_ItemTypeID_N_PriceRange_3000000-0_COVERED*AREA_150m%C2%B2-*',
'https://inmuebles.metroscubicos.com/departamentos/distrito-federal_ItemTypeID_N_PriceRange_3000000-0_COVERED*AREA_90m%C2%B2-150m%C2%B2',
'https://inmuebles.metroscubicos.com/departamentos/distrito-federal_ItemTypeID_U_PriceRange_3000000-0_COVERED*AREA_150m%C2%B2-*',
'https://inmuebles.metroscubicos.com/departamentos/distrito-federal_ItemTypeID_U_PriceRange_3000000-0_COVERED*AREA_90m%C2%B2-150m%C2%B2',
'https://inmuebles.metroscubicos.com/departamentos/distrito-federal_ItemTypeID_U_PriceRange_750000-3000000_COVERED*AREA_60m%C2%B2-80m%C2%B2',
'https://inmuebles.metroscubicos.com/departamentos/distrito-federal_OrderId_PRICE*DESC_PriceRange_0-750000_TOTAL*AREA_*-55m%C2%B2',
'https://inmuebles.metroscubicos.com/departamentos/distrito-federal_OrderId_PRICE*DESC_PriceRange_3000000-0_COVERED*AREA_*-90m%C2%B2',
'https://inmuebles.metroscubicos.com/departamentos/distrito-federal_OrderId_PRICE*DESC_PriceRange_3000000-0_COVERED*AREA_150m%C2%B2-*',
'https://inmuebles.metroscubicos.com/departamentos/distrito-federal_OrderId_PRICE*DESC_PriceRange_3000000-0_COVERED*AREA_150m%C2%B2-*_TOTAL*AREA_*-200m%C2%B2',
'https://inmuebles.metroscubicos.com/departamentos/distrito-federal_OrderId_PRICE*DESC_PriceRange_3000000-0_COVERED*AREA_90m%C2%B2-150m%C2%B2_TOTAL*AREA_*-150m%C2%B2',
'https://inmuebles.metroscubicos.com/departamentos/distrito-federal_OrderId_PRICE*DESC_PriceRange_750000-3000000_COVERED*AREA_*-60m%C2%B2',
'https://inmuebles.metroscubicos.com/departamentos/distrito-federal_OrderId_PRICE*DESC_PriceRange_750000-3000000_COVERED*AREA_60m%C2%B2-80m%C2%B2',
'https://inmuebles.metroscubicos.com/departamentos/distrito-federal_OrderId_PRICE*DESC_PriceRange_750000-3000000_COVERED*AREA_80m%C2%B2-*',
'https://inmuebles.metroscubicos.com/departamentos/distrito-federal_OrderId_PRICE_PriceRange_0-750000_TOTAL*AREA_*-55m%C2%B2',
'https://inmuebles.metroscubicos.com/departamentos/distrito-federal_OrderId_PRICE_PriceRange_3000000-0_COVERED*AREA_*-90m%C2%B2',
'https://inmuebles.metroscubicos.com/departamentos/distrito-federal_OrderId_PRICE_PriceRange_3000000-0_COVERED*AREA_150m%C2%B2-*',
'https://inmuebles.metroscubicos.com/departamentos/distrito-federal_OrderId_PRICE_PriceRange_3000000-0_COVERED*AREA_150m%C2%B2-*_TOTAL*AREA_*-200m%C2%B2',
'https://inmuebles.metroscubicos.com/departamentos/distrito-federal_OrderId_PRICE_PriceRange_3000000-0_COVERED*AREA_90m%C2%B2-150m%C2%B2_TOTAL*AREA_*-150m%C2%B2',
'https://inmuebles.metroscubicos.com/departamentos/distrito-federal_OrderId_PRICE_PriceRange_750000-3000000_COVERED*AREA_*-60m%C2%B2',
'https://inmuebles.metroscubicos.com/departamentos/distrito-federal_OrderId_PRICE_PriceRange_750000-3000000_COVERED*AREA_60m%C2%B2-80m%C2%B2',
'https://inmuebles.metroscubicos.com/departamentos/distrito-federal_OrderId_PRICE_PriceRange_750000-3000000_COVERED*AREA_80m%C2%B2-*',
'https://inmuebles.metroscubicos.com/departamentos/distrito-federal_PriceRange_0-750000_COVERED*AREA_*-75m%C2%B2_TOTAL*AREA_55m%C2%B2-150m%C2%B2',
'https://inmuebles.metroscubicos.com/departamentos/distrito-federal_PriceRange_0-750000_COVERED*AREA_150m%C2%B2-*_TOTAL*AREA_55m%C2%B2-150m%C2%B2',
'https://inmuebles.metroscubicos.com/departamentos/distrito-federal_PriceRange_0-750000_COVERED*AREA_75m%C2%B2-150m%C2%B2_TOTAL*AREA_55m%C2%B2-150m%C2%B2',
'https://inmuebles.metroscubicos.com/departamentos/distrito-federal_PriceRange_0-750000_TOTAL*AREA_150m%C2%B2-*',
'https://inmuebles.metroscubicos.com/departamentos/distrito-federal_PriceRange_3000000-0_COVERED*AREA_150m%C2%B2-*',
'https://inmuebles.metroscubicos.com/departamentos/distrito-federal_PriceRange_3000000-0_COVERED*AREA_150m%C2%B2-*_HAS*GARDEN_242085',
'https://inmuebles.metroscubicos.com/departamentos/distrito-federal_PriceRange_3000000-0_COVERED*AREA_150m%C2%B2-*_HAS*SWIMMING*POOL_242085',
'https://inmuebles.metroscubicos.com/departamentos/distrito-federal_PriceRange_3000000-0_COVERED*AREA_150m%C2%B2-*_TOTAL*AREA_200m%C2%B2-*',
'https://inmuebles.metroscubicos.com/departamentos/distrito-federal_PriceRange_3000000-0_COVERED*AREA_90m%C2%B2-150m%C2%B2_HAS*AIR*CONDITIONING_242085',
'https://inmuebles.metroscubicos.com/departamentos/distrito-federal_PriceRange_3000000-0_COVERED*AREA_90m%C2%B2-150m%C2%B2_HAS*GARDEN_242085',
'https://inmuebles.metroscubicos.com/departamentos/distrito-federal_PriceRange_3000000-0_COVERED*AREA_90m%C2%B2-150m%C2%B2_HAS*GRILL_242085',
'https://inmuebles.metroscubicos.com/departamentos/distrito-federal_PriceRange_3000000-0_COVERED*AREA_90m%C2%B2-150m%C2%B2_HAS*SWIMMING*POOL_242085',
'https://inmuebles.metroscubicos.com/departamentos/distrito-federal_PriceRange_3000000-0_COVERED*AREA_90m%C2%B2-150m%C2%B2_TOTAL*AREA_150m%C2%B2-*',
'https://inmuebles.metroscubicos.com/departamentos/distrito-federal_PriceRange_3000000-0_PublishedToday_YES_COVERED*AREA_150m%C2%B2-*',
'https://inmuebles.metroscubicos.com/departamentos/distrito-federal_PriceRange_3000000-0_PublishedToday_YES_COVERED*AREA_90m%C2%B2-150m%C2%B2',
'https://inmuebles.metroscubicos.com/departamentos/distrito-federal_PriceRange_750000-3000000_COVERED*AREA_60m%C2%B2-80m%C2%B2',
'https://inmuebles.metroscubicos.com/departamentos/distrito-federal_PriceRange_750000-3000000_COVERED*AREA_60m%C2%B2-80m%C2%B2_HAS*SWIMMING*POOL_242085',
'https://inmuebles.metroscubicos.com/departamentos/distrito-federal_PriceRange_750000-3000000_COVERED*AREA_60m%C2%B2-80m%C2%B2_TOTAL*AREA_*-65m%C2%B2',
'https://inmuebles.metroscubicos.com/departamentos/distrito-federal_PriceRange_750000-3000000_COVERED*AREA_60m%C2%B2-80m%C2%B2_TOTAL*AREA_65m%C2%B2-*',
'https://inmuebles.metroscubicos.com/departamentos/distrito-federal_PriceRange_750000-3000000_PublishedToday_YES_COVERED*AREA_60m%C2%B2-80m%C2%B2',
'https://inmuebles.metroscubicos.com/departamentos/estado-de-mexico_OrderId_PRICE*DESC_PriceRange_0-500000',
'https://inmuebles.metroscubicos.com/departamentos/estado-de-mexico_OrderId_PRICE*DESC_PriceRange_500000-5000000',
'https://inmuebles.metroscubicos.com/departamentos/estado-de-mexico_OrderId_PRICE*DESC_PriceRange_5000000-0',
'https://inmuebles.metroscubicos.com/departamentos/estado-de-mexico_OrderId_PRICE_PriceRange_0-500000',
'https://inmuebles.metroscubicos.com/departamentos/estado-de-mexico_OrderId_PRICE_PriceRange_500000-5000000',
'https://inmuebles.metroscubicos.com/departamentos/estado-de-mexico_OrderId_PRICE_PriceRange_5000000-0',
'https://inmuebles.metroscubicos.com/departamentos/guerrero/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/departamentos/guerrero/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/departamentos/hidalgo/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/departamentos/hidalgo/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/departamentos/inmobiliaria/distrito-federal_PriceRange_0-10000',
'https://inmuebles.metroscubicos.com/departamentos/inmobiliaria/distrito-federal_PriceRange_10000-14000',
'https://inmuebles.metroscubicos.com/departamentos/inmobiliaria/distrito-federal_PriceRange_14000-17000',
'https://inmuebles.metroscubicos.com/departamentos/inmobiliaria/distrito-federal_PriceRange_17000-20000',
'https://inmuebles.metroscubicos.com/departamentos/inmobiliaria/distrito-federal_PriceRange_20000-25000',
'https://inmuebles.metroscubicos.com/departamentos/inmobiliaria/distrito-federal_PriceRange_25000-30000',
'https://inmuebles.metroscubicos.com/departamentos/inmobiliaria/distrito-federal_PriceRange_30000-40000',
'https://inmuebles.metroscubicos.com/departamentos/inmobiliaria/distrito-federal_PriceRange_40000-70000',
'https://inmuebles.metroscubicos.com/departamentos/inmobiliaria/distrito-federal_PriceRange_70000-350000',
'https://inmuebles.metroscubicos.com/departamentos/inmobiliaria/distrito-federal_PriceRange_350000-550000',
'https://inmuebles.metroscubicos.com/departamentos/inmobiliaria/distrito-federal_PriceRange_550000-750000',
'https://inmuebles.metroscubicos.com/departamentos/inmobiliaria/distrito-federal_PriceRange_750000-1000000',
'https://inmuebles.metroscubicos.com/departamentos/inmobiliaria/distrito-federal_PriceRange_1000000-1200000',
'https://inmuebles.metroscubicos.com/departamentos/inmobiliaria/distrito-federal_PriceRange_1200000-1500000',
'https://inmuebles.metroscubicos.com/departamentos/inmobiliaria/distrito-federal_PriceRange_1500000-2000000',
'https://inmuebles.metroscubicos.com/departamentos/inmobiliaria/distrito-federal_PriceRange_2000000-2500000',
'https://inmuebles.metroscubicos.com/departamentos/inmobiliaria/distrito-federal_PriceRange_2500000-3000000',
'https://inmuebles.metroscubicos.com/departamentos/inmobiliaria/distrito-federal_PriceRange_3000000-3400000',
'https://inmuebles.metroscubicos.com/departamentos/inmobiliaria/distrito-federal_PriceRange_3400000-3800000',
'https://inmuebles.metroscubicos.com/departamentos/inmobiliaria/distrito-federal_PriceRange_3800000-4200000',
'https://inmuebles.metroscubicos.com/departamentos/inmobiliaria/distrito-federal_PriceRange_4200000-4600000',
'https://inmuebles.metroscubicos.com/departamentos/inmobiliaria/distrito-federal_PriceRange_4600000-5000000',
'https://inmuebles.metroscubicos.com/departamentos/inmobiliaria/distrito-federal_PriceRange_5000000-5500000',
'https://inmuebles.metroscubicos.com/departamentos/inmobiliaria/distrito-federal_PriceRange_5500000-6000000',
'https://inmuebles.metroscubicos.com/departamentos/inmobiliaria/distrito-federal_PriceRange_6000000-6700000',
'https://inmuebles.metroscubicos.com/departamentos/inmobiliaria/distrito-federal_PriceRange_6700000-7500000',
'https://inmuebles.metroscubicos.com/departamentos/inmobiliaria/distrito-federal_PriceRange_7500000-8800000',
'https://inmuebles.metroscubicos.com/departamentos/inmobiliaria/distrito-federal_PriceRange_8800000-11000000',
'https://inmuebles.metroscubicos.com/departamentos/inmobiliaria/distrito-federal_PriceRange_11000000-15000000',
'https://inmuebles.metroscubicos.com/departamentos/inmobiliaria/distrito-federal_PriceRange_15000000-20000000',
'https://inmuebles.metroscubicos.com/departamentos/inmobiliaria/distrito-federal_PriceRange_20000000-35000000',
'https://inmuebles.metroscubicos.com/departamentos/inmobiliaria/distrito-federal_PriceRange_35000000-0',
'https://inmuebles.metroscubicos.com/departamentos/inmobiliaria/distrito-federal_PriceRange_3000000-0_COVERED*AREA_90m%C2%B2-150m%C2%B2',
'https://inmuebles.metroscubicos.com/departamentos/jalisco/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/departamentos/jalisco/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/departamentos/morelos/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/departamentos/morelos/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/departamentos/nuevo-leon/_OrderId_PRICE_PriceRange_0-1500000',
'https://inmuebles.metroscubicos.com/departamentos/nuevo-leon/_OrderId_PRICE_PriceRange_1500000-4500000',
'https://inmuebles.metroscubicos.com/departamentos/nuevo-leon/_OrderId_PRICE_PriceRange_4500000-0',
'https://inmuebles.metroscubicos.com/departamentos/propiedades-individuales/distrito-federal_OrderId_PRICE*DESC_PriceRange_0-2500000',
'https://inmuebles.metroscubicos.com/departamentos/propiedades-individuales/distrito-federal_OrderId_PRICE_PriceRange_0-2500000',
'https://inmuebles.metroscubicos.com/departamentos/propiedades-individuales/distrito-federal_OrderId_PRICE*DESC_PriceRange_2500000-5500000',
'https://inmuebles.metroscubicos.com/departamentos/propiedades-individuales/distrito-federal_OrderId_PRICE_PriceRange_2500000-5500000',
'https://inmuebles.metroscubicos.com/departamentos/propiedades-individuales/distrito-federal_OrderId_PRICE*DESC_PriceRange_5500000-0',
'https://inmuebles.metroscubicos.com/departamentos/propiedades-individuales/distrito-federal_OrderId_PRICE_PriceRange_5500000-0',
'https://inmuebles.metroscubicos.com/departamentos/propiedades-individuales/distrito-federal_OrderId_PRICE_PriceRange_750000-3000000_COVERED*AREA_60m%C2%B2-80m%C2%B2',
'https://inmuebles.metroscubicos.com/departamentos/propiedades-individuales/distrito-federal_OrderId_PRICE*DESC_PriceRange_750000-3000000_COVERED*AREA_60m%C2%B2-80m%C2%B2',
'https://inmuebles.metroscubicos.com/departamentos/propiedades-individuales/distrito-federal_PriceRange_3000000-0_COVERED*AREA_90m%C2%B2-150m%C2%B2',
'https://inmuebles.metroscubicos.com/departamentos/propiedades-individuales/distrito-federal_PriceRange_3000000-4500000_COVERED*AREA_90m%C2%B2-150m%C2%B2',
'https://inmuebles.metroscubicos.com/departamentos/propiedades-individuales/distrito-federal_OrderId_PRICE_PriceRange_4500000-6500000_COVERED*AREA_90m%C2%B2-150m%C2%B2',
'https://inmuebles.metroscubicos.com/departamentos/propiedades-individuales/distrito-federal_OrderId_PRICE*DESC_PriceRange_4500000-6500000_COVERED*AREA_90m%C2%B2-150m%C2%B2',
'https://inmuebles.metroscubicos.com/departamentos/propiedades-individuales/distrito-federal_OrderId_PRICE_PriceRange_6500000-0_COVERED*AREA_90m%C2%B2-150m%C2%B2',
'https://inmuebles.metroscubicos.com/departamentos/propiedades-individuales/distrito-federal_OrderId_PRICE*DESC_PriceRange_6500000-0_COVERED*AREA_90m%C2%B2-150m%C2%B2',
'https://inmuebles.metroscubicos.com/departamentos/puebla/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/departamentos/puebla/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/departamentos/queretaro/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/departamentos/queretaro/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/departamentos/quintana-roo/_OrderId_PRICE*DESC_PriceRange_0-1200000',
'https://inmuebles.metroscubicos.com/departamentos/quintana-roo/_OrderId_PRICE*DESC_PriceRange_1200000-2500000',
'https://inmuebles.metroscubicos.com/departamentos/quintana-roo/_OrderId_PRICE*DESC_PriceRange_2500000-3000000',
'https://inmuebles.metroscubicos.com/departamentos/quintana-roo/_OrderId_PRICE*DESC_PriceRange_3000000-5500000',
'https://inmuebles.metroscubicos.com/departamentos/quintana-roo/_OrderId_PRICE*DESC_PriceRange_5500000-7000000',
'https://inmuebles.metroscubicos.com/departamentos/quintana-roo/_OrderId_PRICE*DESC_PriceRange_7000000-10000000',
'https://inmuebles.metroscubicos.com/departamentos/quintana-roo/_OrderId_PRICE*DESC_PriceRange_10000000-0',
'https://inmuebles.metroscubicos.com/departamentos/quintana-roo/_OrderId_PRICE_PriceRange_0-3000000',
'https://inmuebles.metroscubicos.com/departamentos/quintana-roo/_OrderId_PRICE_PriceRange_3000000-5500000',
'https://inmuebles.metroscubicos.com/departamentos/renta/distrito-federal',
'https://inmuebles.metroscubicos.com/departamentos/renta/distrito-federal_PriceRange_3000000-0_COVERED*AREA_150m%C2%B2-*',
'https://inmuebles.metroscubicos.com/departamentos/renta/distrito-federal_PriceRange_3000000-0_COVERED*AREA_90m%C2%B2-150m%C2%B2',
'https://inmuebles.metroscubicos.com/departamentos/trato-directo/distrito-federal_PriceRange_3000000-0_COVERED*AREA_90m%C2%B2-150m%C2%B2',
'https://inmuebles.metroscubicos.com/departamentos/trato-directo/distrito-federal_PriceRange_750000-3000000_COVERED*AREA_60m%C2%B2-80m%C2%B2',
'https://inmuebles.metroscubicos.com/departamentos/venta/distrito-federal_OrderId_PRICE*DESC_PriceRange_0-1000000',
'https://inmuebles.metroscubicos.com/departamentos/venta/distrito-federal_OrderId_PRICE*DESC_PriceRange_1000000-2000000',
'https://inmuebles.metroscubicos.com/departamentos/venta/distrito-federal_OrderId_PRICE*DESC_PriceRange_2000000-3000000',
'https://inmuebles.metroscubicos.com/departamentos/venta/distrito-federal_OrderId_PRICE*DESC_PriceRange_3000000-4000000',
'https://inmuebles.metroscubicos.com/departamentos/venta/distrito-federal_OrderId_PRICE*DESC_PriceRange_4000000-5000000',
'https://inmuebles.metroscubicos.com/departamentos/venta/distrito-federal_OrderId_PRICE*DESC_PriceRange_5000000-6000000',
'https://inmuebles.metroscubicos.com/departamentos/venta/distrito-federal_OrderId_PRICE*DESC_PriceRange_6000000-7000000',
'https://inmuebles.metroscubicos.com/departamentos/venta/distrito-federal_OrderId_PRICE_PriceRange_0-1000000',
'https://inmuebles.metroscubicos.com/departamentos/venta/distrito-federal_OrderId_PRICE_PriceRange_1000000-2000000',
'https://inmuebles.metroscubicos.com/departamentos/venta/distrito-federal_OrderId_PRICE_PriceRange_100000000-0',
'https://inmuebles.metroscubicos.com/departamentos/venta/distrito-federal_OrderId_PRICE_PriceRange_2000000-3000000',
'https://inmuebles.metroscubicos.com/departamentos/venta/distrito-federal_OrderId_PRICE_PriceRange_3000000-4000000',
'https://inmuebles.metroscubicos.com/departamentos/venta/distrito-federal_OrderId_PRICE_PriceRange_4000000-5000000',
'https://inmuebles.metroscubicos.com/departamentos/venta/distrito-federal_OrderId_PRICE_PriceRange_5000000-6000000',
'https://inmuebles.metroscubicos.com/departamentos/venta/distrito-federal_OrderId_PRICE_PriceRange_6000000-7000000',
'https://inmuebles.metroscubicos.com/departamentos/venta/distrito-federal_OrderId_PRICE_PriceRange_7000000-8000000',
'https://inmuebles.metroscubicos.com/departamentos/venta/distrito-federal_OrderId_PRICE_PriceRange_8000000-10000000',
'https://inmuebles.metroscubicos.com/departamentos/venta/distrito-federal_PriceRange_0-1000000',
'https://inmuebles.metroscubicos.com/departamentos/venta/distrito-federal_PriceRange_3000000-0_COVERED*AREA_150m%C2%B2-*',
'https://inmuebles.metroscubicos.com/departamentos/venta/distrito-federal_PriceRange_3000000-0_COVERED*AREA_90m%C2%B2-150m%C2%B2',
'https://inmuebles.metroscubicos.com/departamentos/veracruz/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/departamentos/veracruz/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/departamentos/yucatan/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/departamentos/yucatan/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/distrito-federal/',
'https://inmuebles.metroscubicos.com/durango/',
'https://inmuebles.metroscubicos.com/durango/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/durango/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/durango/_OrderId_PRICE_PriceRange_0-100000',
'https://inmuebles.metroscubicos.com/durango/_OrderId_PRICE*DESC_PriceRange_0-100000',
'https://inmuebles.metroscubicos.com/durango/_PriceRange_100000-1000000',
'https://inmuebles.metroscubicos.com/durango/_OrderId_PRICE*DESC_PriceRange_1000000-0',
'https://inmuebles.metroscubicos.com/durango/_OrderId_PRICE_PriceRange_1000000-0',
'https://inmuebles.metroscubicos.com/edificios/coahuila/',
'https://inmuebles.metroscubicos.com/edificios/distrito-federal/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/edificios/distrito-federal/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/edificios/estado-de-mexico/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/edificios/estado-de-mexico/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/edificios/guerrero/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/edificios/guerrero/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/edificios/hidalgo/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/edificios/hidalgo/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/edificios/jalisco/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/edificios/jalisco/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/edificios/morelos/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/edificios/morelos/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/edificios/nuevo-leon/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/edificios/nuevo-leon/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/edificios/puebla/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/edificios/puebla/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/edificios/queretaro/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/edificios/queretaro/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/edificios/quintana-roo/',
'https://inmuebles.metroscubicos.com/edificios/veracruz/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/edificios/veracruz/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/edificios/yucatan/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/edificios/yucatan/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/estado-de-mexico/',
'https://inmuebles.metroscubicos.com/estado-de-mexico/_PriceRange_0-10000',
'https://inmuebles.metroscubicos.com/estado-de-mexico/_PriceRange_10000-20000',
'https://inmuebles.metroscubicos.com/estado-de-mexico/_PriceRange_100000-600000',
'https://inmuebles.metroscubicos.com/estado-de-mexico/_PriceRange_1000000-2000000',
'https://inmuebles.metroscubicos.com/estado-de-mexico/_PriceRange_12000000-20000000',
'https://inmuebles.metroscubicos.com/estado-de-mexico/_PriceRange_20000-100000',
'https://inmuebles.metroscubicos.com/estado-de-mexico/_PriceRange_2000000-3000000',
'https://inmuebles.metroscubicos.com/estado-de-mexico/_PriceRange_20000000-0',
'https://inmuebles.metroscubicos.com/estado-de-mexico/_PriceRange_3000000-4000000',
'https://inmuebles.metroscubicos.com/estado-de-mexico/_PriceRange_4000000-6000000',
'https://inmuebles.metroscubicos.com/estado-de-mexico/_PriceRange_600000-1000000',
'https://inmuebles.metroscubicos.com/estado-de-mexico/_PriceRange_6000000-8000000',
'https://inmuebles.metroscubicos.com/estado-de-mexico/_PriceRange_8000000-12000000',
'https://inmuebles.metroscubicos.com/guanajuato/',
'https://inmuebles.metroscubicos.com/guerrero/',
'https://inmuebles.metroscubicos.com/hidalgo/',
'https://inmuebles.metroscubicos.com/jalisco/',
'https://inmuebles.metroscubicos.com/locales-comerciales/chihuahua/',
'https://inmuebles.metroscubicos.com/locales-comerciales/coahuila/',
'https://inmuebles.metroscubicos.com/locales-comerciales/distrito-federal/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/locales-comerciales/distrito-federal/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/locales-comerciales/estado-de-mexico/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/locales-comerciales/estado-de-mexico/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/locales-comerciales/guerrero/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/locales-comerciales/guerrero/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/locales-comerciales/hidalgo/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/locales-comerciales/hidalgo/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/locales-comerciales/jalisco/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/locales-comerciales/jalisco/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/locales-comerciales/morelos/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/locales-comerciales/morelos/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/locales-comerciales/nuevo-leon/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/locales-comerciales/nuevo-leon/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/locales-comerciales/puebla/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/locales-comerciales/puebla/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/locales-comerciales/queretaro/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/locales-comerciales/queretaro/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/locales-comerciales/quintana-roo/',
'https://inmuebles.metroscubicos.com/locales-comerciales/veracruz/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/locales-comerciales/veracruz/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/locales-comerciales/yucatan/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/locales-comerciales/yucatan/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/michoacan/',
'https://inmuebles.metroscubicos.com/morelos/',
'https://inmuebles.metroscubicos.com/nayarit/',
'https://inmuebles.metroscubicos.com/nuevo-leon/',
'https://inmuebles.metroscubicos.com/oaxaca/',
'https://inmuebles.metroscubicos.com/oficinas/coahuila/',
'https://inmuebles.metroscubicos.com/oficinas/distrito-federal_OrderId_PRICE*DESC_PriceRange_10000-95000',
'https://inmuebles.metroscubicos.com/oficinas/distrito-federal_OrderId_PRICE*DESC_PriceRange_95000-0',
'https://inmuebles.metroscubicos.com/oficinas/distrito-federal_OrderId_PRICE_PriceRange_10000-95000',
'https://inmuebles.metroscubicos.com/oficinas/distrito-federal_OrderId_PRICE_PriceRange_95000-0',
'https://inmuebles.metroscubicos.com/oficinas/distrito-federal_PriceRange_0-10000',
'https://inmuebles.metroscubicos.com/oficinas/estado-de-mexico/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/oficinas/estado-de-mexico/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/oficinas/guerrero/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/oficinas/guerrero/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/oficinas/hidalgo/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/oficinas/hidalgo/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/oficinas/jalisco/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/oficinas/jalisco/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/oficinas/morelos/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/oficinas/morelos/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/oficinas/nuevo-leon/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/oficinas/nuevo-leon/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/oficinas/puebla/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/oficinas/puebla/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/oficinas/queretaro/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/oficinas/queretaro/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/oficinas/quintana-roo/',
'https://inmuebles.metroscubicos.com/oficinas/veracruz/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/oficinas/veracruz/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/oficinas/yucatan/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/oficinas/yucatan/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/otros/coahuila/',
'https://inmuebles.metroscubicos.com/otros/distrito-federal/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/otros/distrito-federal/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/otros/estado-de-mexico/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/otros/estado-de-mexico/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/otros/guerrero/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/otros/guerrero/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/otros/hidalgo/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/otros/hidalgo/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/otros/jalisco/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/otros/jalisco/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/otros/morelos/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/otros/morelos/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/otros/nuevo-leon/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/otros/nuevo-leon/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/otros/puebla/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/otros/puebla/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/otros/queretaro/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/otros/queretaro/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/otros/quintana-roo/',
'https://inmuebles.metroscubicos.com/otros/veracruz/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/otros/veracruz/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/otros/yucatan/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/otros/yucatan/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/puebla/',
'https://inmuebles.metroscubicos.com/queretaro/',
'https://inmuebles.metroscubicos.com/quintana-roo/',
'https://inmuebles.metroscubicos.com/ranchos/coahuila/',
'https://inmuebles.metroscubicos.com/ranchos/distrito-federal/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/ranchos/distrito-federal/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/ranchos/estado-de-mexico/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/ranchos/estado-de-mexico/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/ranchos/guerrero/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/ranchos/guerrero/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/ranchos/hidalgo/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/ranchos/hidalgo/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/ranchos/jalisco/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/ranchos/jalisco/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/ranchos/morelos/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/ranchos/morelos/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/ranchos/nuevo-leon/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/ranchos/nuevo-leon/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/ranchos/puebla/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/ranchos/puebla/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/ranchos/queretaro/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/ranchos/queretaro/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/ranchos/quintana-roo/',
'https://inmuebles.metroscubicos.com/ranchos/veracruz/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/ranchos/veracruz/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/ranchos/yucatan/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/ranchos/yucatan/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/renta-vacacional',
'https://inmuebles.metroscubicos.com/renta/guanajuato/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/renta/guanajuato/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/renta/san-luis-potosi/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/renta/san-luis-potosi/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/san-luis-potosi/',
'https://inmuebles.metroscubicos.com/sinaloa/',
'https://inmuebles.metroscubicos.com/sinaloa/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/sinaloa/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/sonora/',
'https://inmuebles.metroscubicos.com/tabasco/',
'https://inmuebles.metroscubicos.com/tabasco/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/tabasco/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/tamaulipas/',
'https://inmuebles.metroscubicos.com/tamaulipas/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/tamaulipas/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/terrenos/coahuila/',
'https://inmuebles.metroscubicos.com/terrenos/distrito-federal/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/terrenos/distrito-federal/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/terrenos/estado-de-mexico_PriceRange_0-1000000',
'https://inmuebles.metroscubicos.com/terrenos/estado-de-mexico_PriceRange_1000000-6500000',
'https://inmuebles.metroscubicos.com/terrenos/estado-de-mexico_PriceRange_6500000-0',
'https://inmuebles.metroscubicos.com/terrenos/guerrero/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/terrenos/guerrero/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/terrenos/hidalgo/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/terrenos/hidalgo/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/terrenos/jalisco/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/terrenos/jalisco/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/terrenos/morelos/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/terrenos/morelos/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/terrenos/nuevo-leon_PriceRange_0-2000000',
'https://inmuebles.metroscubicos.com/terrenos/nuevo-leon_PriceRange_2000000-8500000',
'https://inmuebles.metroscubicos.com/terrenos/nuevo-leon_PriceRange_8500000-0',
'https://inmuebles.metroscubicos.com/terrenos/puebla/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/terrenos/puebla/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/terrenos/queretaro_PriceRange_0-900000',
'https://inmuebles.metroscubicos.com/terrenos/queretaro_PriceRange_3500000-0',
'https://inmuebles.metroscubicos.com/terrenos/queretaro_PriceRange_900000-3500000',
'https://inmuebles.metroscubicos.com/terrenos/quintana-roo/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/terrenos/quintana-roo/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/terrenos/veracruz/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/terrenos/veracruz/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/terrenos/yucatan/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/terrenos/yucatan/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/tlaxcala/',
'https://inmuebles.metroscubicos.com/trasposo',
'https://inmuebles.metroscubicos.com/venta/guanajuato/_OrderId_PRICE',
'https://inmuebles.metroscubicos.com/venta/guanajuato/_OrderId_PRICE*DESC',
'https://inmuebles.metroscubicos.com/venta/san-luis-potosi/_OrderId_PRICE_PriceRange_0-2000000',
'https://inmuebles.metroscubicos.com/venta/san-luis-potosi/_OrderId_PRICE_PriceRange_2000000-4500000',
'https://inmuebles.metroscubicos.com/venta/san-luis-potosi/_OrderId_PRICE_PriceRange_4500000-0',
'https://inmuebles.metroscubicos.com/veracruz/',
'https://inmuebles.metroscubicos.com/yucatan/',
'https://inmuebles.metroscubicos.com/zacatecas/',]
    def __init__(self, *args, **kwargs):
        super(MetroscubicosSpider, self).__init__(*args, **kwargs)
    def response_is_ban(self, request, response):
        # override method
        ban = ("erreur-temporaire" in response.url)
        # ban = (307 == int(response.status))
        url = request.meta.get('redirect_urls', [request.url])[0] if request.meta else request.url
        logger.debug("[RESPONSE_IS_BAN] %s, response.status=%s, response.url=%s, requested url=%s, result_number=%s" %
                     (str(ban),
                      str(response.status),
                      response.url,
                      url,
                      str(response.xpath('//p[contains(@class, "result_number")]//text()'))))
        return ban
   
    """def start_requests(self):
        for url in self.start_urls:
            lower_price = 0
            upper_price = 0
            while upper_price < PRICE_MAX:
                lower_price = upper_price
                upper_price += PRICE_RANGE
                url_with_price = url.format(url, lower=lower_price, upper=upper_price)
                yield scrapy.Request(url=url_with_price, callback=self.parse)"""




    def parse(self, response):
        # scrape ads
        #ads_urls = response.xpath('//a[@class="item-link item__js-link"]/@href').extract()
        ads_urls = response.xpath('//li[@class="ui-search-layout__item"]/div/div/a/@href').extract()
        for url in ads_urls:
            url = url.split("#")[0]
            yield scrapy.Request(url=url, callback=self.parse_ad)
        
        # crawl pages
        #next_page = response.xpath('//li[@class="pagination__next"]/a/@href').extract_first()
        next_page = response.xpath('//li[@class="andes-pagination__button andes-pagination__button--next"]/a/@href').extract_first()
        if next_page:
            yield scrapy.Request(url=next_page, callback=self.parse)
    

    def parse_ad(self, response):
        #d = response.meta['d']
        annonce_link=response.url
        soup = BeautifulSoup(response.text, 'lxml')
        try:
          js=soup.findAll("script")[1].get_text().replace("\r", " ").replace("\n", " ")
          if '"sku"' not in js:
              js=soup.findAll("script")[3].get_text().replace("\r", " ").replace("\n", " ")
              if '"sku"' not in js:
                  js=soup.findAll("script")[4].get_text().replace("\r", " ").replace("\n", " ")
          #print(response.url,js)
        except:
          js=""
        try:
           id_client=js[js.find('"sku"')+7: js.find('"@context"')-2]
           id_client=re.findall('\d+', id_client)[0]

        except:
            pass
        try:
            subtitle=response.xpath("//div[contains(@class,'ui-pdp-header__subtitle')]/span[contains(@class,'ui-pdp-subtitle')]//text()").extract_first()
            categorie=subtitle.split(" en ")[0]
            if subtitle is not None:
              if "Renta" in subtitle:
                achat_loc=2
              elif "Venta" in subtitle:
                achat_loc=1
              else:
                achat_loc=""
            if subtitle is not None:
              if "Desarrollo" in subtitle:
                achat_loc= 1
                neuf_ind = "Y"
              else:
                neuf_ind = ""
            if subtitle is not None:
               if "Locales Comerciales en Traspaso" in subtitle or "Local comercial en Traspaso" in subtitle:
                   achat_loc = 1

        except:
            pass
        try:
            if len(re.findall('Casas', categorie)) > 0:
               maison_apt=1
            elif len(re.findall('Departamentos', categorie)) > 0:
               maison_apt=2
            elif len(re.findall('Terrenos', categorie)) > 0:
               maison_apt=6
            elif len(re.findall('Comerciales', categorie)) > 0:
               maison_apt=5
            else:
               maison_apt=8

        except:
            pass
        try:
           nom=js[js.find('"name"')+8: js.find('"image"')-2]
           nom=nom.replace("\r"," ").replace("\n"," ")
        except:
            pass
        try:
            adresse=response.xpath("//div[contains(@class,'ui-pdp-media ui-vip-location__subtitle ui-pdp-color--BLACK')]/div[contains(@class,'ui-pdp-media__body')]/p[contains(@class,'ui-pdp-color--BLACK ui-pdp-size--SMALL ui-pdp-family--REGULAR ui-pdp-media__title')]//text()").extract_first()
            #adresse=response.xpath("//div[contains(@class,'ui-pdp-media__body')]/p[contains(@class,'ui-pdp-color--BLACK ui-pdp-size--SMALL ui-pdp-family--REGULAR ui-pdp-media__title')]//text()").extract()[-1]
            if len(adresse.split(","))==4:
               quartier = adresse.split(",")[1]
               ville = adresse.split(",")[2]
               province = adresse.split(",")[-1]
            elif len(adresse.split(","))==3:
               quartier = adresse.split(",")[0]
               ville = adresse.split(",")[1]
               province = adresse.split(",")[-1]
            elif len(adresse.split(","))==2:
               quartier = ""
               ville = adresse.split(",")[0]
               province = adresse.split(",")[-1]
            elif len(adresse.split(","))>=5:
               quartier = adresse.split(",")[-3]
               ville = adresse.split(",")[-2]
               province = adresse.split(",")[-1]            
            else:
               quartier = ""
               ville= ""
               province = ""
        except:
            pass
        try:
            annonce_text=' '.join(response.xpath("//p[contains(@class,'ui-pdp-description__content')]//text()").extract())
            annonce_text=annonce_text.replace("\r","").replace("\n","").replace("  ","").replcae(";"," ").replace('"','')
            if len(annonce_text) > 0:
               for i in range(len(annonce_text)):
                  annonce_text[i]=annonce_text[i].replace('\r', '')
                  annonce_text[i]=annonce_text[i].replace('\n', '')
                  annonce_text[i]=annonce_text[i].replace(',', '.')
                  annonce_text[i]=annonce_text[i].replace(';', '.')
                  annonce_text[i]=annonce_text[i].replace('"', '')
                  annonce_text[i]=annonce_text[i].replace('  ', '')
        except:
            pass
        try:
            latitude=response.xpath('//footer/following-sibling::script/text()').re_first(r'"latitude":((-)?(\d+).(\d+)),')
            longitude=response.xpath('//footer/following-sibling::script/text()').re_first(r'"longitude":((-)?(\d+).(\d+))}')
        except:
            pass
        try:
            m2_totale=""
            text=response.xpath('//tr[@class="andes-table__row"]//text()').extract()
            for i in range(0,len(text)):
                if "Superficie construida" in text[i]:
                   m2_totale=text[i+1].split()[0]
                elif "Superficie de construcci" in text[i]:
                   m2_totale=text[i+1].split()[0]

        except:
            m2_totale=""
        try:
            surface_terrain=""
            text=response.xpath('//tr[@class="andes-table__row"]//text()').extract()
            for i in range(0,len(text)):
                if "Superficie total" in text[i]:
                   surface_terrain=text[i+1].split()[0]
          
        except:
            surface_terrain=""
        try:
            nb_garage=""
            text=response.xpath('//tr[@class="andes-table__row"]//text()').extract()
            for i in range(0,len(text)):
                if "Estacionamientos" in text[i]:
                   nb_garage=text[i+1].split()[0]
                    
        except:
            nb_garage=""
        try:
            piece=""
            text=response.xpath('//tr[@class="andes-table__row"]//text()').extract()
            for i in range(0,len(text)):
                if "Recámaras" in text[i]:
                   piece=text[i+1]
                    
            if piece and "-" in piece:
               p = extract_from(piece, '.* - (.*)')
               piece = p
            elif piece and piece=="Mas de 4":
                piece = 5
            elif piece and piece!="Mas de 4" and "-" not in piece:
                piece = piece
            else:
               piece = ""
        except:
            piece=""
        try:
            photo=len(response.xpath('//div[@class="ui-pdp-gallery__column"]/input[@type="radio"]/@value').extract())
        except:
            pass
        try:
           price=js[js.find('"price"')+8: js.find('"availability"')-1]
        except:
            pass
        try:
           agence_nom=response.xpath('//div[@class="ui-vip-profile-info__info-link"]/h3//text()').extract_first()
           #agence_nom=re.findall('"seller_name":{"title":{"text":"(.*)', response.text)[0]
           #agence_nom=agence_nom.split('",')[0]

        except:
            pass
        try:
           mini_site_url =response.xpath('//*[@class="ui-vip-seller-profile__property-link"]/a/@href').extract_first()
           #print("mini_site_url:",response.url,mini_site_url)
        except:
            mini_site_url=""
        try:
          if "CustId_" in mini_site_url:
              mini_site_id =re.findall("CustId_(.*)", mini_site_url)[0]
          else:
               mini_site_id =re.findall('"seller_id":(.*)', response.text)[0]
               mini_site_id=mini_site_id.split(',')[0]

        except:
            mini_site_id=""
        try:
            if mini_site_url is None:
              sellertype="trato-directo"
            else:
              sellertype="inmobiliaria"
        except:
            pass


        d = {'ANNONCE_LINK':annonce_link, 'FROM_SITE':'metroscubicos', 'ID_CLIENT':id_client, 'ANNONCE_DATE':'', 'ACHAT_LOC':achat_loc, 'MAISON_APT':'', 'CATEGORIE':categorie, 'NEUF_IND':neuf_ind, 'NOM':nom, 'ADRESSE':adresse, 'CP':'', 'VILLE':ville, 'QUARTIER':quartier, 'DEPARTEMENT':'', 'REGION':'', 'PROVINCE':province, 'ANNONCE_TEXT':annonce_text, 'ETAGE':'', 'NB_ETAGE':'', 'LATITUDE':latitude, 'LONGITUDE':longitude, 'M2_TOTALE':m2_totale, 'SURFACE_TERRAIN':surface_terrain, 'NB_GARAGE':nb_garage, 'PHOTO':photo, 'PIECE':piece, 'PRIX':price, 'PRIX_M2':'', 'URL_PROMO':'', 'PAYS_AD':'MX', 'PRO_IND':'', 'SELLERTYPE':sellertype, 'MINI_SITE_URL':mini_site_url, 'MINI_SITE_ID':mini_site_id, 'AGENCE_NOM':agence_nom, 'AGENCE_ADRESSE':'', 'AGENCE_CP':'', 'AGENCE_VILLE':'', 'AGENCE_DEPARTEMENT':'', 'EMAIL':'', 'WEBSITE':'', 'AGENCE_TEL':'', 'AGENCE_TEL_2':'', 'AGENCE_TEL_3':'', 'AGENCE_TEL_4':'', 'AGENCE_FAX':'', 'AGENCE_CONTACT':'', 'PAYS_DEALER':'', 'FLUX':'', 'SITE_SOCIETE_URL':'', 'SITE_SOCIETE_ID':'', 'SITE_SOCIETE_NAME':'', 'AGENCE_RCS':'', 'SPIR_ID':''}
        yield d

