# -*- coding: utf-8 -*-

# Scrapy settings for metroscubicos project
#
# For simplicity, this file contains only settings considered important or
# commonly used. You can find more settings consulting the documentation:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#     http://scrapy.readthedocs.org/en/latest/topics/downloader-middleware.html
#     http://scrapy.readthedocs.org/en/latest/topics/spider-middleware.html

BOT_NAME = 'metroscubicos'

SPIDER_MODULES = ['metroscubicos.spiders']
NEWSPIDER_MODULE = 'metroscubicos.spiders'

FEED_EXPORT_FIELDS = ['ANNONCE_LINK', 'FROM_SITE', 'ID_CLIENT', 'ANNONCE_DATE', 'ACHAT_LOC', 'MAISON_APT', 'CATEGORIE', 'NEUF_IND', 'NOM', 'ADRESSE', 'CP', 'VILLE', 'QUARTIER', 'DEPARTEMENT', 'REGION', 'PROVINCE', 'ANNONCE_TEXT', 'ETAGE', 'NB_ETAGE', 'LATITUDE', 'LONGITUDE', 'M2_TOTALE', 'SURFACE_TERRAIN', 'NB_GARAGE', 'PHOTO', 'PIECE', 'PRIX', 'PRIX_M2', 'URL_PROMO', 'PAYS_AD', 'PRO_IND', 'SELLERTYPE', 'MINI_SITE_URL', 'MINI_SITE_ID', 'AGENCE_NOM', 'AGENCE_ADRESSE', 'AGENCE_CP', 'AGENCE_VILLE', 'AGENCE_DEPARTEMENT', 'EMAIL', 'WEBSITE', 'AGENCE_TEL', 'AGENCE_TEL_2', 'AGENCE_TEL_3', 'AGENCE_TEL_4', 'AGENCE_FAX', 'AGENCE_CONTACT', 'PAYS_DEALER', 'FLUX', 'SITE_SOCIETE_URL', 'SITE_SOCIETE_ID', 'SITE_SOCIETE_NAME', 'AGENCE_RCS', 'SPIR_ID']

#FEED_EXPORTERS = {
#    'csv': 'metroscubicos.csv_item_exporter.MyProjectCsvItemExporter'
#}


# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'metroscubicos (+http://www.yourdomain.com)'

# Obey robots.txt rules
#ROBOTSTXT_OBEY = True

# Obey robots.txt rules
ROBOTSTXT_OBEY = False

# Configure maximum concurrent requests performed by Scrapy (default: 16)
#CONCURRENT_REQUESTS = 32

# Configure a delay for requests for the same website (default: 0)
# See http://scrapy.readthedocs.org/en/latest/topics/settings.html#download-delay
# See also autothrottle settings and docs
#DOWNLOAD_DELAY = 0.25
DOWNLOAD_DELAY = 0.25
# The download delay setting will honor only one of:
CONCURRENT_REQUESTS_PER_DOMAIN = 8
CONCURRENT_REQUESTS_PER_IP = 8

# Disable cookies (enabled by default)
#COOKIES_ENABLED = False

# Disable Telnet Console (enabled by default)
#TELNETCONSOLE_ENABLED = False

# Override the default request headers:
#DEFAULT_REQUEST_HEADERS = {
#   'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
#   'Accept-Language': 'en',
#}

# Enable or disable spider middlewares
# See http://scrapy.readthedocs.org/en/latest/topics/spider-middleware.html
#SPIDER_MIDDLEWARES = {
#    'metroscubicos.middlewares.MetroscubicosSpiderMiddleware': 543,
#}

# Enable or disable downloader middlewares
# See http://scrapy.readthedocs.org/en/latest/topics/downloader-middleware.html
#DOWNLOADER_MIDDLEWARES = {
#    'metroscubicos.middlewares.MyCustomDownloaderMiddleware': 543,
#}
RETRY_TIMES = 10 
RETRY_HTTP_CODES = [500, 502, 503, 504, 408, 416, 405, 403, 429, 302, 301, 508, 400]
"""DOWNLOADER_MIDDLEWARES = {
#    'metroscubicos.middlewares.MyCustomDownloaderMiddleware': 543,
#     'metroscubicos.middlewares.proxies': 1,
     'scrapy.downloadermiddlewares.retry.RetryMiddleware': 901
}"""


# Proxy list containing entries like
# http://host1:port
# http://username:password@host2:port
# http://host3:port
# ...
#PROXY_LIST = '/home/t.ammar/list.txt'

# Proxy mode
# 0 = Every requests have different proxy
# 1 = Take only one proxy from the list and assign it to every requests
# 2 = Put a custom proxy to use in the settings
#PROXY_MODE = 0

DOWNLOADER_MIDDLEWARES = {
    # ...
    #'httpIP.middlewares.RotateUserAgentMiddleware': 20,
    'scrapy.downloadermiddlewares.retry.RetryMiddleware': 90,
    'rotating_proxies.middlewares.RotatingProxyMiddleware': 610,
    #'rotating_proxies.middlewares.BanDetectionMiddleware': 620,
    # ...
}

#stromproxy
ROTATING_PROXY_LIST=[
"172.16.2.14:3128"
]
# Enable or disable extensions
# See http://scrapy.readthedocs.org/en/latest/topics/extensions.html
#EXTENSIONS = {
#    'scrapy.extensions.telnet.TelnetConsole': None,
#}

# Configure item pipelines
# See http://scrapy.readthedocs.org/en/latest/topics/item-pipeline.html
ITEM_PIPELINES = {
    'metroscubicos.pipelines.MetroscubicosPipeline': 300,
}

# Enable and configure the AutoThrottle extension (disabled by default)
# See http://doc.scrapy.org/en/latest/topics/autothrottle.html
#AUTOTHROTTLE_ENABLED = True
# The initial download delay
#AUTOTHROTTLE_START_DELAY = 5
# The maximum download delay to be set in case of high latencies
#AUTOTHROTTLE_MAX_DELAY = 60
# The average number of requests Scrapy should be sending in parallel to
# each remote server
#AUTOTHROTTLE_TARGET_CONCURRENCY = 1.0
# Enable showing throttling stats for every response received:
#AUTOTHROTTLE_DEBUG = False

# Enable and configure HTTP caching (disabled by default)
# See http://scrapy.readthedocs.org/en/latest/topics/downloader-middleware.html#httpcache-middleware-settings
import time
LOG_ENABLED = True
LOG_LEVEL = 'DEBUG'
crawl_time = time.strftime("%Y%m%d_%Hh-%Mmin")
HTTPCACHE_ENABLED = True
HTTPCACHE_EXPIRATION_SECS = 0
HTTPCACHE_DIR ='/home/t.ammar/Metro/cachesWare'
HTTPCACHE_IGNORE_HTTP_CODES = [500, 502, 503, 504, 408, 416, 405, 403, 429, 302, 301, 508, 400, 404]
HTTPCACHE_STORAGE = 'scrapy.extensions.httpcache.FilesystemCacheStorage'
HTTPCACHE_IGNORE_MISSING = False
LOG_FILE = "/home/t.ammar/Metro/metroscubicos/spiders/logs/%s_%s.log" %('metros', crawl_time) 
FEED_URI = "%s_%s.csv"%('metroscubicos',crawl_time)
FEED_FORMAT = "csv"



# -----------------------------------------------------------------------------
# USER AGENT
# -----------------------------------------------------------------------------

DOWNLOADER_MIDDLEWARES.update({
        'scrapy.downloadermiddlewares.useragent.UserAgentMiddleware': None,
            'scrapy_useragents.downloadermiddlewares.useragents.UserAgentsMiddleware': 500,
            })

USER_AGENTS = [
    ('Mozilla/5.0 (X11; Linux x86_64) '
     'AppleWebKit/537.36 (KHTML, like Gecko) '
     'Chrome/57.0.2987.110 '
     'Safari/537.36'),  # chrome
    ('Mozilla/5.0 (X11; Linux x86_64) '
     'AppleWebKit/537.36 (KHTML, like Gecko) '
     'Chrome/61.0.3163.79 '
     'Safari/537.36'),  # chrome
    ('Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:55.0) '
     'Gecko/20100101 '
     'Firefox/55.0')  # firefox
]

