#! /usr/bin/python3.5
import csv
from csv import QUOTE_ALL

f = open('all_metroscubicos_11_v2.csv', 'w')
with open('all_metroscubicos_11_v1.csv') as csvfile:
    f2 = csv.writer(f, delimiter=';', quotechar='"',quoting=QUOTE_ALL)

    spamreader = csv.reader(csvfile, delimiter=';', quotechar='"')

    for row in spamreader:
        if len (row) == 54:
            if "ha" in row[21]:    
                row[21] = str(row[21]).replace("ha","").rstrip()
            if "ha" in row[22]:    
                row[22] = str(row[22]).replace("ha","").rstrip()
            if "m" in row[21]:
                row[21] = str(row[21]).replace("m","").rstrip()
            if "m" in row[22]:    
                row[22] = str(row[22]).replace("m","").rstrip()
            if row[6] == "Departamento":
                row[5] = "2"
            elif row[6] == "Local comercial":
                row[5] = "17"
            elif row[6] == "Quinta":
                row[5] = "1"
            elif row[6] == "Bodega":
                row[5] = "16"
            elif row[6] == "Casa":
                row[5] = "1"
            elif row[6] == "Otro inmueble":
                row[5] = "8"
            elif row[6] == "Cuarto":
                row[5] = "2"
            elif row[6] == "Oficina":
                row[5] = "19"
            elif row[6] == "Rancho":
                row[5] = "33"
            elif row[6] == "Terreno":
                row[5] = "6"
            elif row[6] == "Edificio":
                row[5] = "7"
            if row [31] == "inmobiliaria":
                row[30] = "Y"
            elif row [31] == "trato-directo":
                row[30] = "N"
            if "PRIX" not in row[26]:
               prix_1 = row[26].replace("$","").replace(",","").strip()
               prix_1=prix_1.split(".")[0]
               #prix_2 = int(prix_1/19)
               prix_2 = int(prix_1)/19
               row[26] =  prix_2

            f2.writerow(row)
        else:
            pass

